@extends('layout')

@section('title')

Обо мне

@endsection

@section('main_content')

<div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
        <h1 class="display-4 fst-italic">Обо Мне</h1>
        <p class="lead my-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum non perferendis sunt minus ullam soluta placeat ducimus dolorum! Accusamus a iure aut non ipsam qui eum inventore ducimus officia voluptatem.</p>
        <p class="lead mb-0"><a href="#" class="text-white fw-bold">Читать далее...</a></p>
    </div>
</div>

@endsection 